import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class NavigatorButton extends StatelessWidget {
  const NavigatorButton({Key? key}) : super(key: key);
  // var _navigator = [];

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 3,
      padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
      mainAxisSpacing: 10,
      crossAxisSpacing: 10,
      children: [
        Container(
            height: 90,
            width: 120,
            //color: Colors.white,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.grain,
                    size: 45,
                    color: Colors.black,
                  ),
                  FittedBox(
                    fit: BoxFit.cover,
                    child: Text('ARRAÇOAMENTO',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        )),
                  )
                ],
              ),
              onPressed: () {
                Modular.to.navigate('./Arrac/');
              },
            )),
        Container(
          height: 90,
          width: 120,
          //color: Colors.white,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20), color: Colors.white),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(
                  Icons.panorama_photosphere_outlined,
                  size: 45,
                  color: Colors.black,
                ),
                Text(
                  'BIOMETRIA',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                )
              ],
            ),
            onPressed: () {
              Modular.to.navigate('./Bio/');
            },
          ),
        ),
        Container(
            height: 90,
            width: 120,
            //color: Colors.white,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.report_problem_sharp,
                    size: 45,
                    color: Colors.black,
                  ),
                  FittedBox(
                    fit: BoxFit.cover,
                    child: Text('MORTALIDADE',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        )),
                  )
                ],
              ),
              onPressed: () {
                Modular.to.navigate('./Mort/');
              },
            )),
        Container(
            height: 90,
            width: 120,
            //color: Colors.white,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.stars_rounded,
                    size: 45,
                    color: Colors.black,
                  ),
                  FittedBox(
                    fit: BoxFit.cover,
                    child: Text('QUALIDADE DA ÁGUA',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        )),
                  ),
                ],
              ),
              onPressed: () {
                Modular.to.navigate('./Qual/');
              },
            )),
        Container(
          height: 90,
          width: 120,
          //color: Colors.white,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.white),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(
                  Icons.wifi_protected_setup_outlined,
                  size: 45,
                  color: Colors.black,
                ),
                FittedBox(
                  fit: BoxFit.cover,
                  child: Text('SINCRONIZAR',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      )),
                )
              ],
            ),
            onPressed: () {
              Modular.to.pushNamed('./Sinc/');
            },
          ),
        ),
        Container(
          height: 90,
          width: 120,
          //color: Colors.white,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.white),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(
                  Icons.settings_outlined,
                  size: 45,
                  color: Colors.black,
                ),
                FittedBox(
                  fit: BoxFit.cover,
                  child: Text(
                    'CONFIGURAÇÃO',
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
            onPressed: () {
              Modular.to.pushNamed('./Conf/');
            },
          ),
        ),
        Container(
          height: 90,
          width: 120,
          //color: Colors.white,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.white),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(
                  Icons.dashboard,
                  size: 45,
                  color: Colors.black,
                ),
                Text(
                  'Dashboard',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                )
              ],
            ),
            onPressed: () {
              Modular.to.pushNamed('./Dash');
            },
          ),
        ),
      ],
    );
  }
}
