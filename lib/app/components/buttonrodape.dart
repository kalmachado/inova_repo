import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ButtonRodape extends StatelessWidget {
  const ButtonRodape({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        // Botão de contato, que vai p/ pagina de contatos
        Column(
          children: [
            IconButton(
              icon: const Icon(Icons.phone_outlined, color: Colors.white),
              //iconSize: 35,
              onPressed: () {
                Modular.to.navigate('./Contato');
              },
            ),
            const Text(
              'Contato',
              style: TextStyle(
                //fontSize: 18,
                color: Colors.white,
              ),
            ),
          ],
        ),
        // Botão de sair, que volta para tela Login
        Column(
          children: [
            IconButton(
              icon: const Icon(Icons.exit_to_app_outlined, color: Colors.white),
              //iconSize: 35,
              onPressed: () {
                Modular.to.navigate('/');
              },
            ),
            const Text(
              'Sair',
              style: TextStyle(
                color: Colors.white,
                //fontSize: 18,
              ),
            )
          ],
        ),
      ],
    );
  }
}
