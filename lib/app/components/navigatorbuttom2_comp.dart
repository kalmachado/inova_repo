import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class NavigatorText extends StatelessWidget {
  const NavigatorText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {
                Modular.to.navigate('./Arrac/CadastroArracoamento');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const <Widget>[
                  Text('Número de tanques > '),
                  Text('419 tanques '),
                ],
              )),
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {
                Modular.to.navigate('./Arrac/CadastroArracoamento');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('TCA >'),
                  Text('1,46'),
                ],
              )),
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {
                Modular.to.navigate('./Arrac/CadastroArracoamento');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Último Arraçoamento >'),
                  Text('8.609,583 kg'),
                ],
              )),
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {
                Modular.to.navigate('./Mort/CadastroMort');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Mortalidade >'),
                  Text('14%'),
                ],
              )),
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {
                Modular.to.navigate('./Mort/CadastroMort');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Quantidade >'),
                  Text('4.011.727 und'),
                ],
              )),
          TextButton(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Biomassa >'),
                  Text('706.238 kg'),
                ],
              )),
        ],
      ),
    );
  }
}
