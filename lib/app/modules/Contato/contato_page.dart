import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class Contato extends StatelessWidget {
  const Contato({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contato'),
        leading: TextButton(
          child: const Text('X',
              style: TextStyle(color: Colors.white, fontSize: 26)),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: SizedBox(
              child: Image.asset('assets/images/inova_logo.jpeg'),
            ),
          ),
          const SizedBox(height: 25),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/images/whatsapp_icon.jpeg'),
              const SizedBox(width: 10),
              const Text(
                '+55 (19) 3565-4389',
                style: TextStyle(fontSize: 20),
              )
            ],
          ),
          const SizedBox(height: 25),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(Icons.phone_outlined),
              SizedBox(width: 15),
              Text(
                '+55 (19) 3565-4389',
                style: TextStyle(fontSize: 20),
              )
            ],
          ),
          const SizedBox(height: 25),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.email_outlined,
                size: 35,
              ),
              SizedBox(width: 15),
              Text(
                'atendimento@agroinova.com.br',
                style: TextStyle(fontSize: 20),
              )
            ],
          )
        ],
      ),
    );
  }
}
