import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/dashboard/dashboard_store.dart';
import 'package:flutter/material.dart';
import 'package:inova_treino/app/modules/dashboard/lotes_dashboard.dart';
import 'package:inova_treino/app/modules/dashboard/und_lotes.dart';

class DashboardPage extends StatefulWidget {
  final String title;
  const DashboardPage({Key? key, this.title = 'Dashboard'}) : super(key: key);
  @override
  DashboardPageState createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage> {
  final DashboardStore store = Modular.get();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Expanded(
              child: DashBoardLotes(),
            ),
            store.buildBullets(),
            Container(
              height: size.height * 0.45,
              padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
              color: Colors.blue[900],
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Text("Detalhes",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      )),
                  SizedBox(
                    height: 255,
                    child: UnidLotes(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
