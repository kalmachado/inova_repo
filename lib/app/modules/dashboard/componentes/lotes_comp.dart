class Lotes {
  final String title;
  final double biomassa;
  final double pesoMedio;
  final double qntd;
  final double retirados;
  final double tca;
  final int id;

  Lotes({
    required this.id,
    required this.title,
    required this.biomassa,
    required this.pesoMedio,
    required this.qntd,
    required this.retirados,
    required this.tca,
  });
}
