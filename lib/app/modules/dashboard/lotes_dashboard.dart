import 'package:flutter/material.dart';
import 'package:inova_treino/app/modules/dashboard/componentes/lotes_comp.dart';
import 'package:carousel_slider/carousel_slider.dart';

class DashBoardLotes extends StatefulWidget {
  const DashBoardLotes({Key? key}) : super(key: key);

  @override
  _DashBoardLotesState createState() => _DashBoardLotesState();
}

class _DashBoardLotesState extends State<DashBoardLotes> {
  int current = 0;
  final lotesList = [
    Lotes(
      id: 1,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 2,
      title: 'Lote 02',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 3,
      title: 'Lote 03',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 4,
      title: 'Lote 04',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 5,
      title: 'Lote 05',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 6,
      title: 'Lote 06',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return SizedBox(
      height: size.height,
      width: size.width,
      child: Column(
        children: [
          CarouselSlider(
            options: CarouselOptions(
              height: 269,
              enlargeCenterPage: true,
              initialPage: 0,
              autoPlay: false,
              viewportFraction: 0.4,
              enableInfiniteScroll: false,
            ),
            items: lotesList.map((si) {
              //* Definição do tamanho e cor da lista
              return Builder(builder: (context) {
                return FittedBox(
                  fit: BoxFit.cover,
                  child: Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      height: 270,
                      width: 160,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.purple,
                        ),
                        onPressed: () {},
                        child: Padding(
                          padding: const EdgeInsets.all(8),
                          //* Itens mostrados na caixa da lista.
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    si.title,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: const <Widget>[
                                  Text("Biomassa: ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(si.biomassa.toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                ],
                              ),
                              Row(
                                children: const <Widget>[
                                  Text("Peso medio: ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(si.pesoMedio.toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                ],
                              ),
                              Row(
                                children: const <Widget>[
                                  Text("Quantidade: ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(si.qntd.toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                ],
                              ),
                              Row(
                                children: const <Widget>[
                                  Text("Retirados: ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(si.retirados.toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                ],
                              ),
                              Row(
                                children: const <Widget>[
                                  Text("TCA: ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(si.tca.toString(),
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                ],
                              )
                            ],
                          ),
                        ),
                      )),
                );
              });
            }).toList(),
          ),
        ],
      ),
    );
  }
}
