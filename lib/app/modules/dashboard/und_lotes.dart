import 'package:flutter/material.dart';
import 'package:inova_treino/app/modules/dashboard/componentes/lotes_comp.dart';

class UnidLotes extends StatefulWidget {
  const UnidLotes({Key? key}) : super(key: key);

  @override
  _UnidLotesState createState() => _UnidLotesState();
}

class _UnidLotesState extends State<UnidLotes> {
  final _lotesund1 = [
    Lotes(
      id: 1,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 2,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 3,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 4,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 5,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
    Lotes(
      id: 6,
      title: 'Lote 01',
      biomassa: 16570.80,
      pesoMedio: 1343.80,
      qntd: 12.338,
      retirados: 7.143,
      tca: 1.51,
    ),
  ];
  // final _lotesund2 = [
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  //   Lotes(
  // id: 1
  //     title: 'Lote 02',
  //     biomassa: 16570.80,
  //     pesoMedio: 1343.80,
  //     qntd: 12.338,
  //     retirados: 7.143,
  //     tca: 1.51,
  //   ),
  // ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.45,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 1,
          itemBuilder: (context, index) {
            return Row(
                children: _lotesund1.map((si) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.green,
                  ),
                  padding: const EdgeInsets.all(8),
                  height: 250,
                  width: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            si.title,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text("Biomassa: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(si.biomassa.toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text("Peso medio: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(si.pesoMedio.toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text("Quantidade: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(si.qntd.toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text("Retirados: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(si.retirados.toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 18)),
                        ],
                      ),
                      Row(
                        children: const <Widget>[
                          Text("TCA: ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(si.tca.toString(),
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 18)),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }).toList());
          }),
    );
  }
}
// 