import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/biometria/biometria_store.dart';
import 'package:flutter/material.dart';

class BiometriaPage extends StatefulWidget {
  final String title;
  const BiometriaPage({Key? key, this.title = 'BiometriaPage'})
      : super(key: key);
  @override
  BiometriaPageState createState() => BiometriaPageState();
}

class BiometriaPageState extends State<BiometriaPage> {
  final BiometriaStore store = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Biometria'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: SizedBox(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Qtd. de peixes >'),
                Text('130 und'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Peso Médio >'),
                Text('176,043 g'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Biomassa >'),
                Text('706.238,273 kg'),
              ],
            ),
          ),
          const SizedBox(height: 400),
          SizedBox(
            height: 60,
            width: 370,
            child: ElevatedButton(
              child: const Text(
                'CADASTRAR',
                style: TextStyle(fontSize: 25),
              ),
              onPressed: () {
                Modular.to.navigate('./CadastroBio');
              },
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.blue[800])),
            ),
          ),
        ],
      )),
    );
  }
}
