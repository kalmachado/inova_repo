import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/biometria/componentes/bio_cadastro.dart';
import 'package:inova_treino/app/modules/biometria/componentes/bio_cadastro_realizado.dart';

class CadastroBio extends StatefulWidget {
  const CadastroBio({Key? key}) : super(key: key);

  @override
  _CadastroBioState createState() => _CadastroBioState();
}

class _CadastroBioState extends State<CadastroBio>
    with SingleTickerProviderStateMixin {
  TabController? _controller;

  @override
  // ignore: must_call_super
  void initState() {
    _controller = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cadastrar Biometria'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/Bio/');
          },
        ),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'Data de Execução',
                  style: TextStyle(fontSize: 20),
                ),
                const SizedBox(height: 10),
                const Text('30/09/2021', style: TextStyle(fontSize: 18)),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: TextFormField(
                    enabled: false,
                    decoration: InputDecoration(
                        label: const Text('Pesquisas'),
                        prefixIcon:
                            const Icon(Icons.search, color: Colors.black),
                        suffixIcon: IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.list_sharp))),
                  ),
                ),
                //* Barra de navegação entre guias
                Column(
                  children: <Widget>[
                    TabBar(
                        controller: _controller,
                        labelColor: Colors.black,
                        tabs: const <Widget>[
                          Tab(text: 'A realizar'),
                          Tab(text: 'Realizado'),
                        ]),
                    SizedBox(
                      height: 397,
                      child: TabBarView(
                        controller: _controller,
                        children: <Widget>[
                          //* Primeira Janela ("A Realizar")
                          SizedBox(
                              child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                              const SizedBox(height: 10),
                              BioArealizar(),
                            ]),
                          )),
                          //* Segunda Janela ("Realizado")
                          SizedBox(
                            child: SingleChildScrollView(
                              child: Column(children: const <Widget>[
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                                SizedBox(height: 10),
                                BioRealizado(),
                              ]),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ]),
        ),
      ),
    );
  }
}
