import 'package:inova_treino/app/modules/biometria/biometria_page.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/biometria/cadastro_bio.dart';

class BiometriaModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const BiometriaPage()),
    ChildRoute('/CadastroBio', child: (context, args) => const CadastroBio()),
  ];
}
