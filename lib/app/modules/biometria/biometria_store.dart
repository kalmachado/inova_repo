import 'package:mobx/mobx.dart';

part 'biometria_store.g.dart';

class BiometriaStore = _BiometriaStoreBase with _$BiometriaStore;
abstract class _BiometriaStoreBase with Store {

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  } 
}