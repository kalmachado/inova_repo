// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'configuracao_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ConfiguracaoStore on ConfiguracaoStoreBase, Store {
  final _$agrupandoAtom = Atom(name: 'ConfiguracaoStoreBase.agrupando');

  @override
  bool get agrupando {
    _$agrupandoAtom.reportRead();
    return super.agrupando;
  }

  @override
  set agrupando(bool value) {
    _$agrupandoAtom.reportWrite(value, super.agrupando, () {
      super.agrupando = value;
    });
  }

  final _$ConfiguracaoStoreBaseActionController =
      ActionController(name: 'ConfiguracaoStoreBase');

  @override
  void agroup() {
    final _$actionInfo = _$ConfiguracaoStoreBaseActionController.startAction(
        name: 'ConfiguracaoStoreBase.agroup');
    try {
      return super.agroup();
    } finally {
      _$ConfiguracaoStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
agrupando: ${agrupando}
    ''';
  }
}
