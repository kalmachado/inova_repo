import 'package:mobx/mobx.dart';

part 'configuracao_store.g.dart';

class ConfiguracaoStore = ConfiguracaoStoreBase with _$ConfiguracaoStore;

abstract class ConfiguracaoStoreBase with Store {
  @observable
  bool agrupando = false;

  @action
  void agroup() {
    agrupando = true;
  }
}
