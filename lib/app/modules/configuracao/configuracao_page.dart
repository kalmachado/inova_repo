import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/configuracao/configuracao_store.dart';
import 'package:flutter/material.dart';

class ConfiguracaoPage extends StatefulWidget {
  final String title;
  const ConfiguracaoPage({Key? key, this.title = 'ConfiguracaoPage'})
      : super(key: key);
  @override
  ConfiguracaoPageState createState() => ConfiguracaoPageState();
}

class ConfiguracaoPageState extends State<ConfiguracaoPage> {
  final ConfiguracaoStoreBase store = Modular.get<ConfiguracaoStoreBase>();
  bool agrupando = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 15, bottom: 10),
              child: Text(
                'Tarefas',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.blue[800],
                ),
              ),
            ),
            Observer(
              builder: (_) {
                return SwitchListTile(
                  title: Text(
                    'Agrupar arraçoamentos e retiradas',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.blue[600],
                    ),
                  ),
                  value: agrupando,
                  onChanged: (bool value) {
                    setState(() {
                      agrupando = value;
                    });
                  },
                );
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Container(
                  height: 2, width: double.infinity, color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
