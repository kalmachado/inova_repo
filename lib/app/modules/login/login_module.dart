import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/home/home_module.dart';
import 'package:inova_treino/app/modules/home/home_store.dart';
import 'login_page.dart';

class LoginModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => HomeStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute,
        child: (_, args) => const LoginPage(title: '')),
    ModuleRoute('/Home', module: HomeModule()),
  ];
}
