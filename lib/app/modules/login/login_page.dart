import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/components/botao_help_components.dart';
import 'package:inova_treino/app/modules/login/login_store.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key? key, this.title = 'LoginPage'}) : super(key: key);
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final LoginStore store = Modular.get<LoginStore>();

  bool _senha = true;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 20),
            Row(children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 25),
                child: SizedBox(
                  width: 335,
                  height: 150,
                  child: Image.asset('assets/images/seja_bem_vindo.png'),
                ),
              ),
              const BotaoHelp(),
            ]),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                decoration: const InputDecoration(
                  labelText: ('Endereço na internet'),
                  suffixText: ('.inovawebapp.com.br'),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                decoration: const InputDecoration(
                  labelText: ('Usuario'),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextField(
                obscureText: _senha,
                decoration: InputDecoration(
                  labelText: ('Senha'),
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        _senha = !_senha;
                      });
                    },
                    icon: const Icon(Icons.remove_red_eye_outlined),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 60),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: SizedBox(
                height: 50,
                width: double.infinity,
                child: ElevatedButton(
                    style: const ButtonStyle(),
                    onPressed: () {
                      Modular.to.navigate('/Home/');
                    },
                    child:
                        const Text('ENTRAR', style: TextStyle(fontSize: 20))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
