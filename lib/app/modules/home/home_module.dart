import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/Contato/contato_page.dart';
import 'package:inova_treino/app/modules/arracoamento/arracoamento_module.dart';
import 'package:inova_treino/app/modules/arracoamento/arracoamento_store.dart';
import 'package:inova_treino/app/modules/biometria/biometria_module.dart';
import 'package:inova_treino/app/modules/biometria/biometria_store.dart';
import 'package:inova_treino/app/modules/configuracao/configuracao_module.dart';
import 'package:inova_treino/app/modules/configuracao/configuracao_store.dart';
import 'package:inova_treino/app/modules/dashboard/dashboard_module.dart';
import 'package:inova_treino/app/modules/mortalidade/mortalidade_module.dart';
import 'package:inova_treino/app/modules/mortalidade/mortalidade_store.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/qualidade_da_agua_module.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/qualidade_da_agua_store.dart';
import 'package:inova_treino/app/modules/sincronizar/sincronizar_module.dart';
import 'package:inova_treino/app/modules/sincronizar/sincronizar_store.dart';
import 'home_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => SincronizarStore()),
    Bind.lazySingleton((i) => ArracoamentoStore()),
    Bind.lazySingleton((i) => ConfiguracaoStore()),
    Bind.lazySingleton((i) => QualidadeDaAguaStore()),
    Bind.lazySingleton((i) => MortalidadeStore()),
    Bind.lazySingleton((i) => BiometriaStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const HomePage()),
    ModuleRoute('/Arrac', module: ArracoamentoModule()),
    ModuleRoute('/Bio', module: BiometriaModule()),
    ModuleRoute('/Mort', module: MortalidadeModule()),
    ModuleRoute('/Qual', module: QualidadeDaAguaModule()),
    ModuleRoute('/Sinc', module: SincronizarModule()),
    ModuleRoute('/Conf', module: ConfiguracaoModule()),
    ModuleRoute('/Dash', module: DashboardModule()),
    ChildRoute('/Contato', child: (_, args) => const Contato()),
  ];
}
