import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/components/botao_help_components.dart';
import 'package:inova_treino/app/components/buttonrodape.dart';
import 'package:inova_treino/app/components/navigatorbuttom2_comp.dart';
import 'package:inova_treino/app/components/navigatorbuttom_comp.dart';
import 'package:inova_treino/app/modules/home/home_store.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key? key, this.title = 'HomePage'}) : super(key: key);
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends ModularState<HomePage, HomeStore> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        children: [
          //* Barra de cabeçaho
          Expanded(
            flex: 1,
            child: SizedBox(
              // sheight: 110,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    //* Logo
                    child: SizedBox(
                      height: 60,
                      width: 65,
                      child: Image.asset('assets/images/logo_home.jpeg'),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 5, bottom: 5, right: 30),
                    //* Botao de tela
                    child: SizedBox(
                      height: 100,
                      width: 210,
                      child: ListView(
                        children: const [
                          ExpansionTile(
                            title: Text('GS Santa Fé do Sul'),
                            children: [
                              ListTile(
                                title: Text('Centro P&D'),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  //* Botão de ajuda
                  const Padding(
                    padding: EdgeInsets.only(top: 5, left: 10, right: 10),
                    child: BotaoHelp(),
                  ),
                ],
              ),
            ),
          ),
          //* Linha de divisão
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 5),
          //   child: Container(
          //     height: 2,
          //     width: size.width,
          //     color: Colors.black,
          //   ),
          // ),
          //* campo onde ficarao os dados a serem exibidos.
          Expanded(
            flex: 4,
            child: Container(
              // height: 255,
              width: size.width,
              decoration: const BoxDecoration(
                borderRadius:
                    BorderRadius.vertical(bottom: Radius.circular(20)),
                color: Colors.white,
              ),
              //* Botão de navegação dos TextButton.
              child: const NavigatorText(),
            ),
          ),
          //* Campo de navegação entre as demais paginas.
          Expanded(
            flex: 4,
            child: Container(
              // height: 245,
              width: size.width,
              color: Colors.blue[900],
              // Botão do menu de navegação.
              child: const NavigatorButton(),
            ),
          ),
          //* barra de rodapé.
          Expanded(
            flex: 1,
            child: Container(
              // height: 71,
              width: size.width,
              color: Colors.blue[700],
              child: const ButtonRodape(),
            ),
          ),
        ],
      ),
    );
  }
}
