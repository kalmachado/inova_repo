import 'package:flutter/material.dart';

class MenuItens extends StatelessWidget {
  const MenuItens({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        padding: const EdgeInsets.only(top: 20, bottom: 105),
        onPressed: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  const Text('L013-031'),
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Container(
                        height: 1, width: double.infinity, color: Colors.black),
                  ),
                ],
              )),
              content: SizedBox(
                  height: 80,
                  width: 350,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const <Widget>[
                          Text('Versão do Aplicativo'),
                          Text('2.9.0'),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const <Widget>[
                          Text('Atualização em'),
                          Text('14/10/2021'),
                        ],
                      )
                    ],
                  )),
              actions: <Widget>[
                Center(
                  child: SizedBox(
                    width: 130,
                    height: 50,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.grey[50]),
                      onPressed: () => Navigator.pop(
                        context,
                      ),
                      child: const Text('OK',
                          style: TextStyle(color: Colors.black, fontSize: 20)),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        icon: const Icon(Icons.help_outline_outlined, color: Colors.black54),
        iconSize: 45);
  }
}
