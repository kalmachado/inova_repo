import 'package:flutter/material.dart';
import 'package:inova_treino/app/modules/sincronizar/componentes/edit_item.dart';
import 'package:inova_treino/app/modules/sincronizar/componentes/sincronizacao_comp.dart';
import 'package:intl/intl.dart';

class SincronizarItens extends StatefulWidget {
  const SincronizarItens({Key? key}) : super(key: key);

  @override
  _SincronizarItensState createState() => _SincronizarItensState();
}

class _SincronizarItensState extends State<SincronizarItens> {
  final _sincronizacao = [
    Sincron(
      id: 'S1',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.grain,
        color: Colors.white,
        size: 40,
      ),
      info: 'Arraçoamento',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.grain,
        color: Colors.white,
        size: 40,
      ),
      info: 'Arraçoamento',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.panorama_photosphere_outlined,
        color: Colors.white,
        size: 40,
      ),
      info: 'Biometria',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.report_problem_sharp,
        color: Colors.white,
        size: 40,
      ),
      info: 'Mortalidade',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.stars_rounded,
        color: Colors.white,
        size: 40,
      ),
      info: 'Qualidade da agua',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.stars_rounded,
        color: Colors.white,
        size: 40,
      ),
      info: 'Qualidade da agua',
    ),
    Sincron(
      id: 'S2',
      title: 'L013-031',
      date: DateTime.now(),
      value: '15.29 kg',
      icone: const Icon(
        Icons.stars_rounded,
        color: Colors.white,
        size: 40,
      ),
      info: 'Qualidade da agua',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: _sincronizacao.map((si) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: SizedBox(
              child: Card(
                elevation: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, bottom: 10, top: 10),
                      child: SizedBox(
                        height: 75,
                        width: 80,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.blue[900],
                                    borderRadius: BorderRadius.circular(50)),
                                height: 50,
                                width: 50,
                                child: si.icone,
                              ),
                            ),
                            const SizedBox(height: 5),
                            FittedBox(child: Text(si.info!)),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(si.title,
                                  style: const TextStyle(fontSize: 18)),
                              const SizedBox(height: 5),
                              Text(DateFormat('d MMM y').format(si.date),
                                  style: const TextStyle(fontSize: 16)),
                              const SizedBox(height: 5),
                              Text('Quantidade: ${si.value}',
                                  style: const TextStyle(fontSize: 16))
                            ])),
                    const SizedBox(width: 80),
                    Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: PopupMenuButton(
                          onSelected: (value) {
                            setState(() {
                              const MenuItens();
                            });
                          },
                          child: const Icon(Icons.more_vert),
                          itemBuilder: (context) {
                            return <PopupMenuEntry<Widget>>[
                              PopupMenuItem(
                                child: const Text('Editar'),
                                onTap: () => const MenuItens(),
                              ),
                              const PopupMenuItem(child: Text('Excluir'))
                            ];
                          },
                        ))
                  ],
                ),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}
