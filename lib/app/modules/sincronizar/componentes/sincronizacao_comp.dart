import 'package:flutter/material.dart';

class Sincron {
  final String id;
  final String title;
  final String value;
  final DateTime date;
  final Icon? icone;
  final String? info;

  Sincron(
      {required this.id,
      required this.title,
      required this.date,
      required this.value,
      required this.icone,
      required this.info});
}
