import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/sincronizar/componentes/sincro_comp.dart';
import 'package:flutter/material.dart';

import 'sincronizar_store.dart';

class SincronizarPage extends StatefulWidget {
  final String title;
  const SincronizarPage({Key? key, this.title = 'SincronizarPage'})
      : super(key: key);
  @override
  SincronizarPageState createState() => SincronizarPageState();
}

class SincronizarPageState extends State<SincronizarPage> {
  final SincronizarStore store = Modular.get<SincronizarStore>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sincronismo'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(bottom: 25),
        child: SizedBox(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.75,
                child: const SincronizarItens(),
              ),
              const SizedBox(height: 10),
              //* Botão de sincronização!!
              SizedBox(
                height: 55,
                child: ElevatedButton(
                  onPressed: () {
                    Modular.to.navigate('/Home/');
                  },
                  child: const Text('Sicronizar'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
