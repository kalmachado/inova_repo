import 'package:flutter_modular/flutter_modular.dart';
import 'sincronizar_page.dart';

class SincronizarModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const SincronizarPage(title: '')),
  ];
}
