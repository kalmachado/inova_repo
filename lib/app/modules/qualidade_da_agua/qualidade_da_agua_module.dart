import 'package:inova_treino/app/modules/qualidade_da_agua/cadrastrar_medicao.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/qualidade_da_agua_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class QualidadeDaAguaModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const QualidadeDaAguaPage()),
    ChildRoute('/CadastroAgua', child: (_, args) => const CadastroMedicao())
  ];
}
