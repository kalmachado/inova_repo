import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/qualidade_da_agua_store.dart';
import 'package:flutter/material.dart';

class QualidadeDaAguaPage extends StatefulWidget {
  final String title;
  const QualidadeDaAguaPage({Key? key, this.title = 'QualidadeDaAguaPage'})
      : super(key: key);
  @override
  QualidadeDaAguaPageState createState() => QualidadeDaAguaPageState();
}

class QualidadeDaAguaPageState extends State<QualidadeDaAguaPage> {
  final QualidadeDaAguaStore store = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Qualidade da água'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: SizedBox(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Oxigênio >'),
                Text('0 mg/L'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Temperatura >'),
                Text('0 °C'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('pH >'),
                Text('0'),
              ],
            ),
          ),
          const SizedBox(height: 400),
          SizedBox(
            height: 60,
            width: 370,
            child: ElevatedButton(
              child: const Text(
                'CADASTRAR',
                style: TextStyle(fontSize: 25),
              ),
              onPressed: () {
                Modular.to.navigate('./CadastroAgua');
              },
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.blue[800])),
            ),
          ),
        ],
      )),
    );
  }
}
