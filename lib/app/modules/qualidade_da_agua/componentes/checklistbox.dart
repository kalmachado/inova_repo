import 'package:flutter/material.dart';

import 'lista.dart';

class BoxList extends StatefulWidget {
  BoxList({Key? key}) : super(key: key);

  final itens = [
    TipoList(title: 'P004-L06', mod: false),
    TipoList(title: 'L001-001', mod: false),
    TipoList(title: 'L001-200', mod: false),
    TipoList(title: 'L001-TR011', mod: false),
    TipoList(title: 'L001-TR012', mod: false),
    TipoList(title: 'L012-023', mod: false),
    TipoList(title: 'P004-L06', mod: false),
    TipoList(title: 'L001-001', mod: false),
    TipoList(title: 'L001-200', mod: false),
    TipoList(title: 'L001-TR011', mod: false),
    TipoList(title: 'L001-TR012', mod: false),
    TipoList(title: 'L012-023', mod: false),
    TipoList(title: 'P004-L06', mod: false),
    TipoList(title: 'L001-001', mod: false),
    TipoList(title: 'L001-200', mod: false),
    TipoList(title: 'L001-TR011', mod: false),
    TipoList(title: 'L001-TR012', mod: false),
    TipoList(title: 'L012-023', mod: false),
  ];

  @override
  _BoxListState createState() => _BoxListState();
}

class _BoxListState extends State<BoxList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 230,
      width: 380,
      decoration: BoxDecoration(border: Border.all(width: 1)),
      child: ListView.builder(
        itemCount: widget.itens.length,
        itemBuilder: (_, index) {
          final item = widget.itens[index];
          return SizedBox(
            height: 35,
            child: CheckboxListTile(
                controlAffinity: ListTileControlAffinity.leading,
                title: Text(item.title),
                key: Key(item.title),
                value: item.mod,
                onChanged: (value) {}),
          );
        },
      ),
    );
  }
}
