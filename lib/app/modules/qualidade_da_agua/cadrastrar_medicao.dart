import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/componentes/checklistbox.dart';

class CadastroMedicao extends StatefulWidget {
  const CadastroMedicao({Key? key}) : super(key: key);

  @override
  _CadastroMedicaoState createState() => _CadastroMedicaoState();
}

class _CadastroMedicaoState extends State<CadastroMedicao> {
  bool allChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cadastro de Medição'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/Qual/');
          },
        ),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 5),
          child: SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    'Data de Execução',
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(height: 10),
                  const Text('30/09/2021 10:01',
                      style: TextStyle(fontSize: 18)),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: SizedBox(
                        child: Column(children: [
                      Row(children: [
                        SizedBox(
                          width: 340,
                          child: TextFormField(
                            enabled: false,
                            decoration: const InputDecoration(
                              label: Text('Oxigênio'),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(top: 30),
                          child: Text('mg/L', style: TextStyle(fontSize: 20)),
                        )
                      ]),
                      Row(children: [
                        SizedBox(
                          width: 150,
                          child: TextFormField(
                            enabled: false,
                            decoration: const InputDecoration(
                                labelText: ('Temperatura')),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 5, right: 5, top: 30),
                          child: Text('°C', style: TextStyle(fontSize: 20)),
                        ),
                        SizedBox(
                          width: 150,
                          child: TextFormField(
                            enabled: false,
                            decoration:
                                const InputDecoration(labelText: ('pH')),
                          ),
                        ),
                      ]),
                    ])),
                  ),
                  const SizedBox(height: 10),
                  Center(
                    child: SizedBox(
                      width: 380,
                      child: Column(
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                const Text(
                                  "Tanque: ",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 50),
                                  child: SizedBox(
                                    height: 40,
                                    width: 208,
                                    child: CheckboxListTile(
                                      title: const Text('Selecionar todos'),
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      value: false,
                                      onChanged: (bool? value) {
                                        setState(() {});
                                      },
                                    ),
                                  ),
                                )
                              ]),
                          BoxList(),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 45),
                  Center(
                    child: SizedBox(
                      height: 55,
                      width: 370,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.blue[800]),
                          onPressed: () {
                            Modular.to.navigate('/Home/Qual/');
                          },
                          child: const Text('SALVAR',
                              style: TextStyle(fontSize: 20))),
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
