// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'qualidade_da_agua_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QualidadeDaAguaStore on _QualidadeDaAguaStoreBase, Store {
  final _$_QualidadeDaAguaStoreBaseActionController =
      ActionController(name: '_QualidadeDaAguaStoreBase');

  @override
  void increment() {
    final _$actionInfo = _$_QualidadeDaAguaStoreBaseActionController
        .startAction(name: '_QualidadeDaAguaStoreBase.increment');
    try {
      return super.increment();
    } finally {
      _$_QualidadeDaAguaStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
