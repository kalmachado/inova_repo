// import 'package:inova_treino/app/modules/qualidade_da_agua/componentes/lista.dart';
import 'package:mobx/mobx.dart';

part 'qualidade_da_agua_store.g.dart';

class QualidadeDaAguaStore = _QualidadeDaAguaStoreBase
    with _$QualidadeDaAguaStore;

abstract class _QualidadeDaAguaStoreBase with Store {
  @observable
  @action
  void increment() {
    //  value = true;
  }
}
