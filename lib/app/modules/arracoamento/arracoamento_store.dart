import 'package:mobx/mobx.dart';

part 'arracoamento_store.g.dart';

class ArracoamentoStore = _ArracoamentoStoreBase with _$ArracoamentoStore;

abstract class _ArracoamentoStoreBase with Store {
  @observable
  bool menu = true;

  @action
  void trocarMenu() {
    menu = false;
  }
}
