// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'arracoamento_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ArracoamentoStore on _ArracoamentoStoreBase, Store {
  final _$menuAtom = Atom(name: '_ArracoamentoStoreBase.menu');

  @override
  bool get menu {
    _$menuAtom.reportRead();
    return super.menu;
  }

  @override
  set menu(bool value) {
    _$menuAtom.reportWrite(value, super.menu, () {
      super.menu = value;
    });
  }

  final _$_ArracoamentoStoreBaseActionController =
      ActionController(name: '_ArracoamentoStoreBase');

  @override
  void trocarMenu() {
    final _$actionInfo = _$_ArracoamentoStoreBaseActionController.startAction(
        name: '_ArracoamentoStoreBase.trocarMenu');
    try {
      return super.trocarMenu();
    } finally {
      _$_ArracoamentoStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
menu: ${menu}
    ''';
  }
}
