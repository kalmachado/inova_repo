import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/arracoamento/componentes/arracoamento_comp.dart';
import 'package:inova_treino/app/modules/arracoamento/arracoamento_store.dart';
import 'package:flutter/material.dart';

class ArracoamentoPage extends StatefulWidget {
  final String title;
  const ArracoamentoPage({Key? key, this.title = 'ArracoamentoPage'})
      : super(key: key);
  @override
  ArracoamentoPageState createState() => ArracoamentoPageState();
}

class ArracoamentoPageState extends State<ArracoamentoPage> {
  final ArracoamentoStore store = Modular.get();

  get children => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Arraçoamento'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Modular.to.navigate('/Home/');
            },
          ),
        ),
        body: Column(
          children: [
            //* Cabeça da pagina
            SizedBox(
              height: 90,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: const [
                      Text("Numero de Tanques >"),
                    ],
                  ),
                  Row(
                    children: const [
                      Text('Total de rações >'),
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  Icon(
                    Icons.scatter_plot_sharp,
                    size: 25,
                  ),
                  SizedBox(width: 5),
                  Text(
                    'Esperando por ração',
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            //* Lista do corpo da pagna
            const SizedBox(
              height: 400,
              width: double.infinity,
              child: ArracoamentoList(),
            ),
            //* Botão de cadastro
            SizedBox(
              height: 60,
              width: 370,
              child: ElevatedButton(
                child: const Text(
                  'CADASTRAR',
                  style: TextStyle(fontSize: 25),
                ),
                onPressed: () {
                  Modular.to.navigate('./CadastroArracoamento');
                },
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color?>(Colors.blue[800])),
              ),
            ),
          ],
        ));
  }
}
