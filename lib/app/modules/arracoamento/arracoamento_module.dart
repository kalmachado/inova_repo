import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/arracoamento/cadastro_arracoamento.dart';

import 'arracoamento_page.dart';

class ArracoamentoModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const ArracoamentoPage()),
    ChildRoute('/CadastroArracoamento',
        child: (context, args) => const CadastroArracoamento()),
  ];
}
