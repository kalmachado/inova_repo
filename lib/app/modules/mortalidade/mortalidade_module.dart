import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/mortalidade/cadastro_mortalidade.dart';
import 'mortalidade_page.dart';

class MortalidadeModule extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => const MortalidadePage()),
    ChildRoute('/CadastroMort',
        child: (_, args) => const CadastroMortalidade()),
  ];
}
