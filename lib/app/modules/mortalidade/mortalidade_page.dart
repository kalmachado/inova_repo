import 'package:flutter_modular/flutter_modular.dart';
import 'package:inova_treino/app/modules/mortalidade/mortalidade_store.dart';
import 'package:flutter/material.dart';

class MortalidadePage extends StatefulWidget {
  final String title;
  const MortalidadePage({Key? key, this.title = 'MortalidadePage'})
      : super(key: key);
  @override
  MortalidadePageState createState() => MortalidadePageState();
}

class MortalidadePageState extends State<MortalidadePage> {
  final MortalidadeStore store = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mortalidade'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Modular.to.navigate('/Home/');
          },
        ),
      ),
      body: SizedBox(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Registros >'),
                Text('0'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text('Biomassa >'),
                Text('0'),
              ],
            ),
          ),
          const SizedBox(height: 400),
          SizedBox(
            height: 60,
            width: 370,
            child: ElevatedButton(
              child: const Text(
                'CADASTRAR',
                style: TextStyle(fontSize: 25),
              ),
              onPressed: () {
                Modular.to.navigate('./CadastroMort');
              },
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.blue[800])),
            ),
          ),
        ],
      )),
    );
  }
}
