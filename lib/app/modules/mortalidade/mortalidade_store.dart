import 'package:mobx/mobx.dart';

part 'mortalidade_store.g.dart';

class MortalidadeStore = _MortalidadeStoreBase with _$MortalidadeStore;
abstract class _MortalidadeStoreBase with Store {

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  } 
}