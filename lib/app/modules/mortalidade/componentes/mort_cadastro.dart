// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class MortArealizar extends StatelessWidget {
  MortArealizar({Key? key}) : super(key: key);

  String? dropdownValue = 'teste 1';

  @override
  Widget build(BuildContext context) {
    return
        //* Item da lista
        SizedBox(
      child: Column(children: [
        //* Titulo do Item
        Container(
          height: 55,
          width: 370,
          decoration: BoxDecoration(
            color: Colors.blue[900],
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(8),
              topRight: Radius.circular(8),
            ),
          ),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: const [
                Text('P004-L06   ',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                SizedBox(width: 25),
                Icon(
                  Icons.check_circle_outline,
                  color: Colors.green,
                  size: 38,
                )
              ]),
        ),
        //* Conteudo do Item
        Container(
          height: 100,
          width: 370,
          decoration: BoxDecoration(
            color: Colors.blue[700],
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('724 und',
                      style: TextStyle(color: Colors.green[300], fontSize: 20)),
                  Text('646,52 kg',
                      style: TextStyle(color: Colors.green[300], fontSize: 20)),
                  Text('892,99 g',
                      style: TextStyle(color: Colors.green[300], fontSize: 20)),
                ],
              ),
            ),
            Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10, right: 5),
                child: Row(
                  children: const <Widget>[
                    Text(
                      'Mortalidade Coletada ',
                      style: TextStyle(color: Colors.white),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  children: [
                    Column(
                      children: const <Widget>[
                        Text('    0    ',
                            style: TextStyle(
                                fontSize: 22,
                                color: Colors.white,
                                decoration: TextDecoration.underline)),
                      ],
                    ),
                    const Text(' und ',
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    const Icon(
                      Icons.arrow_drop_down,
                      color: Colors.white,
                    )
                  ],
                ),
              )
            ]),
          ]),
        )
      ]),
    );
  }

  void setState(Null Function() param0) {}
}
