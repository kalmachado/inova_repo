import 'package:flutter_test/flutter_test.dart';
import 'package:inova_treino/app/modules/mortalidade/mortalidade_store.dart';
 
void main() {
  late MortalidadeStore store;

  setUpAll(() {
    store = MortalidadeStore();
  });

  test('increment count', () async {
    expect(store.value, equals(0));
    store.increment();
    expect(store.value, equals(1));
  });
}