import 'package:modular_test/modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inova_treino/app/modules/qualidade_da_agua/qualidade_da_agua_module.dart';

void main() {
  setUpAll(() {
    initModule(QualidadeDaAguaModule());
  });
}
