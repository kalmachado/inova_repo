import 'package:modular_test/modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inova_treino/app/modules/home/home_module.dart';

void main() {
  setUpAll(() {
    initModule(HomeModule());
  });
}
