import 'package:flutter_test/flutter_test.dart';
import 'package:inova_treino/app/modules/biometria/biometria_store.dart';
 
void main() {
  late BiometriaStore store;

  setUpAll(() {
    store = BiometriaStore();
  });

  test('increment count', () async {
    expect(store.value, equals(0));
    store.increment();
    expect(store.value, equals(1));
  });
}